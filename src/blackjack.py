import random
from card import Card
from deck import Deck
from hand import Hand

class Blackjack:
    def __init__(self):
        pass

    def play(self):
        playing = True
        
        while playing:
            self.deck = Deck()
            self.deck.shuffle()
            self.p_hand = Hand()
            self.d_hand = Hand(dealer=True)

            for i in range(2):
                self.p_hand.deal_card(self.deck.deal())
                self.d_hand.deal_card(self.deck.deal())
            print("Player hand:")
            self.p_hand.display()
            print()
            print("Dealer hand:")
            self.d_hand.display()
            game_over = False

            while not game_over:
                p_blackjack, d_blackjack = self.blackjack_check()
                if p_blackjack or d_blackjack:
                    game_over = True
                    self.blackjack_results(p_blackjack, d_blackjack)
                    continue

                choice = input("Do you want to hit or Stand?").lower()
                while choice not in ["h", "s", "hit", "stand"]:
                    choice = input("Please enter 'hit' 'stand' 'H' or 'S' ").lower()
                if choice in ['hit', 'h']:
                    self.p_hand.deal_card(self.deck.deal())
                    self.p_hand.display()
                    if self.player_gameover():
                        print("Player lost!")
                        game_over = True
                else:
                    p_hand_value = self.p_hand.get_value()
                    d_hand_value = self.d_hand.get_value()
                    print("Player Hand:", p_hand_value)
                    print("Dealer Hand:", d_hand_value)

                    if p_hand_value > d_hand_value:
                        print("Player Wins.")
                    elif p_hand_value == d_hand_value:
                        print("Tie.")
                    else:
                        print("Dealer Wins.")
                    game_over = True
            
            again = input("Play Again? Yes or No ")
            while again.lower() not in ["y", "n" , "yes" , "no" ]:
                again = input("Please enter 'yes' 'no' 'y' or 'n' ")
            if again.lower() in ['no', 'n']:
                playing = False
            else:
                game_over = False

    def player_gameover(self):
        return self.p_hand.get_value() > 21

    def blackjack_check(self):
        #test comment 
        player = False
        dealer = False
        if self.p_hand.get_value() == 21:
            player = True
        if self.d_hand.get_value() == 21:
            dealer = True
        return player, dealer

    def blackjack_results(self, p_blackjack, d_blackjack):
        if p_blackjack and d_blackjack:
            print("Blackjack Draw.")
        elif p_blackjack:
            print("Player blackjack! Player wins.")
        elif d_blackjack:
            print("Dealer blackjack! Dealer wins.")

if __name__ == "__main__":
    g = Blackjack()
    g.play()