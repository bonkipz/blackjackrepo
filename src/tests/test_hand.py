import unittest
from blackjack import Blackjack
from hand import Hand
from deck import Deck
from card import Card


class TestHand(unittest.TestCase):

    def setUp(self):
        self.hand = Hand(dealer=False)

    def test_player_false(self):
        self.assertFalse(self.hand.dealer)

    def test_empty_hand(self):
        self.assertEqual(self.hand.cards, [])


if __name__ == "__main__":
    unittest.main()