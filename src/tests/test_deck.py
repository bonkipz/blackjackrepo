import unittest
from blackjack import Blackjack
from hand import Hand
from deck import Deck
from card import Card


class TestDeck(unittest.TestCase):

    def setUp(self):
        self.deck = Deck()

    def test_deal_true(self):
        self.assertEqual(self.deck.deal().__repr__(),"A of Spades")

    def test_deal_false(self):
        self.assertNotEqual(self.deck.deal().__repr__(),"K of Diamonds")

    def test_deck(self):
        self.assertNotEqual(self.deck.cards, [])
if __name__ == "__main__":
    unittest.main()