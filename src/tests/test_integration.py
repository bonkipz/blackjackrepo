import unittest
from unittest.mock import patch
from blackjack import Blackjack
from hand import Hand
from deck import Deck
from card import Card


class TestIntegration(unittest.TestCase):
    def setUp(self):
        self.deck = Deck()
        self.hand = Hand()
        self.game = Blackjack()
        self.dealer_hand = Hand(dealer=True)

    @patch('builtins.input', side_effect=['s', 'n'])
    def test_play_game(self, mock_input):
        self.playing = True
        self.game.play()
        self.assertEqual(self.playing, True)
        
    def test_deck_card(self):
        self.assertEqual(self.deck.deal().__repr__(),"A of Spades")
  
    def test_hand(self):
        self.hand.cards.append(Card("Spades", "A"))
        self.assertEqual(self.hand.cards[0].value, "A")

    def test_show_hand(self):
        self.hand.cards.append(Card("Diamonds", "K"))
        self.hand.cards.append(Card("Spades", "A"))
        self.assertIsNot(self.hand.display(), "")

    def test_total(self):
        self.hand.cards.append(Card("Diamonds", "K"))
        self.hand.cards.append(Card("Spades", "A"))
        self.hand.calculate_value()
        self.assertNotEqual(self.hand.get_value(), 0)




if __name__ == "__main__":
    unittest.main()