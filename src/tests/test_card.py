import unittest
import os, sys
sys.path.append(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))

from blackjack import Blackjack
from hand import Hand
from deck import Deck
from card import Card

class TestCard(unittest.TestCase):

    def setUp(self):
        self.card = Card("Hearts", "K")

    def test_repr_true(self):
        self.assertEqual(self.card.__repr__(), "K of Hearts")

    def test_repr_false(self):
        self.assertNotEqual(self.card.__repr__(), "6 of Clubs")     

    def test_suit_true(self):
        self.assertEqual(self.card.suit, "Hearts")

    def test_value_false(self):
        self.assertNotEqual(self.card.value, "6")

if __name__ == "__main__":
    unittest.main()