# README #

## CS 491 Final Project ##
### Esteban Pina-Lira ###
This is repository for final project. I chose black jack from previous homework assignment to automate unit testing, deployment, and integration.
I used bitbucket yml pipeline to automate every step from unit testing, integration testing, code styling, and program source deployment.

This repository will run every unit test for the python program including integration test it will display code coverage after everything has ran.
Once unit testing has been completed a code styling checker will run to ensure consistant styling throughout the project.
Once prerequisites are met with unit testing and code styling it will deploy the program to a public docker image.

